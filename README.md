# (DEPRECATED) system/security

> **Warning!**
> That role is deprecated and replaced by:
> 
> 1. [ssh](https://gitlab.com/etke.cc/roles/ssh)
> 2. [fail2ban](https://gitlab.com/etke.cc/roles/fail2ban)
> 3. [ufw](https://gitlab.com/etke.cc/roles/ufw)

That role manages security measures of a linux system/host

* fail2ban
* ufw
* ssh

> **NOTE**: check [defaults/main.yml](./defaults/main.yml) to see full list of config options
